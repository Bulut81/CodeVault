
# ==================================================================================================
# This file is part of the CodeVault project. The project is licensed under Apache Version 2.0.
# CodeVault is part of the EU-project PRACE-4IP (WP7.3.C).
#
# Author(s):
#   Evghenii Gaburov <evghenii.gaburov@surfsara.nl>
#
# ==================================================================================================

# CMake project
cmake_minimum_required(VERSION 3.0.0 FATAL_ERROR)
project("BHTree" NONE)
include(${CMAKE_CURRENT_SOURCE_DIR}/../../cmake/common.cmake)

# ==================================================================================================

if ("${DWARF_PREFIX}" STREQUAL "")
  set(DWARF_PREFIX 4_nbody)
endif()
set(NAME ${DWARF_PREFIX}_bhtree)

enable_language(CXX)
if(NOT "${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU")
  message("## Skipping '${NAME}': requires GNU C++ compiler ")
  install(CODE "MESSAGE(\"bhtree can only be built with GNU C++ compiler.\")")
  return()
endif()
message("** Enabling '${NAME}'")

include(CheckCXXCompilerFlag)
CHECK_CXX_COMPILER_FLAG("-std=c++11" COMPILER_SUPPORTS_CXX11)
set(CXX11 ${COMPILER_SUPPORTS_CXX11})
set(CXX11_FLAGS -std=c++11)
 
find_package(OpenMP)

macro(build_openmp)
  set_property(SOURCE src/bhtree_omp.cpp APPEND_STRING PROPERTY COMPILE_FLAGS " ${OpenMP_CXX_FLAGS}")
  add_executable(${NAME}_omp src/bhtree_omp.cpp)
  set_target_properties(${NAME}_omp PROPERTIES LINK_FLAGS "${OpenMP_CXX_FLAGS}")
  install(TARGETS ${NAME}_omp DESTINATION bin)
endmacro(build_openmp)

find_package(Common)
if (NOT CXX11)
  message("## Skipping ${NAME}: no C++11 support")
  dummy_install(${NAME} "C++11")
  return()
endif()

message("** Enabling ${NAME} example")
add_definitions(${CXX11_FLAGS} -Wall -Werror)
add_executable(${NAME} src/bhtree.cpp)
install(TARGETS ${NAME} DESTINATION bin)

find_package(OpenMP)

if (NOT ${OPENMP_FOUND})
  message("## Skipping ${NAME}: no OpenMP support found")
else (NOT ${OPENMP_FOUND})
  message("** Enabling ${NAME} OpenMP examples")
  build_openmp()
endif()


# ==================================================================================================
