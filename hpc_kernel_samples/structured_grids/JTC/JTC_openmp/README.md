=======
README
=======
# 1. Code sample name
Jacobi Test Code (JTC): OpenMP version

# 2. Description of the code sample package
This code sample provides an implementation of the Jacobi algorithm that iterates a 7 points stencil on a cuboid grid.

* The initial grid state is an eigenvector of the Jacobi smoother
    `sin(pi*kx*x/L)*sin(pi*ky*y/L)sin(pi*kz*z/L)`.

* The numerical kernel is applied to the grid for a selected number of\
iterations.  Timing data is collected over several runs, each with\
the same number of iterations in order to avoid hardware transient\
effects.

* The average time per iteration is collected for each run and\
the minimum, the average and the maximum values over the runs are\
reported. The output also contains the values of the grid sizes, block sizes (if the loops are blocked), the number of used OpenMP threads.

# 3. Release date
21 Nov 2015


# 4. Version history
This code sample is based on v1.0.5b (8\super th\nosupersub  December 2014)


# 5. Contributor (s) / Maintainer(s)
Andrew Sunderland, email: andrew.sunderland@stfc.ac.uk

# 6. Copyright / License of the code sample
None. The code is free to copy, modify and use, but comes with no warranty and therefore usage is at your own risk.

# 7. Language(s)
The code sample files are all written in C. No external libraries are required.

# 8. Parallelisation Implementation(s)
OpenMP

# 9. Level of the code sample complexity \
The code sample complexity is moderate and is not intended for new starters to parallel programming. 
Users should already be familiar with C, C preprocessing and OpenMP usage


# 10. Instructions on how to compile the code
* Load a support programming environment or compiler [GNU, PGI, Intel, Clang]

* Use the CodeVault CMake infrastructure, see main README.md

# 11. Instructions on how to run the code
The following command line options can be used to set the  run parameters:

* -ng nx ny nz       set the global grid sizes
* -nb bx by bz       set the computational block size - relevant for blocked algorithms in OpenMP
* -nruns __n__   number of runs (default 5)
* -niter __b__   number of full iteration (swaps) over the grid per run (default 1).
           Note: if wave algorithm is used niter is fixed to num-wave(see below)
                 and this flag is ignored. \
* -t test the correctness; if enabled the executable collects the norms
	of the vectors (grid functions) at the end of each run and prints
	the diference between the ratio of the norms of two consecutive
	runs and the Jacobi smoother eigenvalue to the power niter. This
	quantity should be small (machine precision), theoretically is
	0. This works because the initial vector on the grid is an
	eigenvector of the Jacobi smoother. Warning: Numerical artefacts
	may creep in for large number of iterations and small grids.

* -cmodel  __name__
     selects the compute model implementation.
   __name__ can take one of the following values:
     openmp
     mpi+omp (option to be added to Codevault at a later date).

* -alg __name__
       selects an algorithm for the Jacobi iteration. Each compute model has at least
       one algorithm, named baseline. Obviously, baseline is not the same algorithm
	   for all compute models.
	   
	  Other algorithms:
	  ---------------
          baseline-opt - used in kernel Titanium_laplace3d which is Gold_laplace3d with
                        basic loop optimisations ( hoisted if, faster index algebra). Implemented for MPI+OpenMP and OpenMP.
          blocked - uses the blocked loops version, implemented for MPI+OpenMP and OpenMP.
         cco - implemented in MPI+OpenMP, uses master thread for halo exchange.
		          __Note__: the Jacobi loops don't use vec1D function to help vectorisation in this version.
          wave num-waves threads-per-column - time skewed blocks
	      Notes:
	      ------
              num-waves is the number of waves used in one grid swap
              threads-per-column is the number of threads used on each columns (< number of waves)
                                 if the wave is applied in yz plane, the blocks have local
                                 domain length in x direction.

          2d-blockgrid - uses 2d CUDA grids, each block of threads loops in z direction
          3d-blockgrid - uses 3d CUDA grids, the block of threads have size 1 in z
                         direction, hence grid size is nz in z. ( this also CUDA baseline)
          gpu-bandwidth - measures the time for the simple update u[i] = const * v[i]
                         useful to measure the effective GPU bandwidth.
                         Note: -t is meaningless in this case

          help : prints the list of available algorithms.

          Notes:
	     -----
	      1. Default model is baseline.
 
* -pc       prints information on the run parameters.
* -nh       output timings without the table header (this useful when collecting large data sets for plots)
* -malign __n__  use `posix_memalign` to allocate the working array with an address alignment
             of __n__ bytes.
             It may help vectorisation on certain systems. Default allocation is done with malloc.
* -help        prints a short description of command line arguments
* -version   prints version

# 12. Sample input(s)
Input-data is generated automatically when running the program.

# 13. Sample output(s)
* Last norm  1.116329895927438e+04
* ================================================================================
*     nThs        Nx        Ny        Nz     NITER      minT     meanT      maxT 
* ================================================================================
*         4      1000      1000      1000      21  3.166e+00 3.180e+00 3.220e+00